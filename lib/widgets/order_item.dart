import 'package:flutter/material.dart';
import '../providers/orders.dart' as o;
import 'package:intl/intl.dart';
import 'dart:math';

class OrderItem extends StatefulWidget{
  final o.OrderItem order;

  OrderItem(this.order);

  @override
  _OrderItemState createState() => _OrderItemState();
}

class _OrderItemState extends State<OrderItem> {

  bool _isExpended = false;

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Card(
      margin: EdgeInsets.symmetric(
        horizontal: 10,
        vertical: 5,
      ),
      child: Column(
        children: [
          ListTile(
            title: Text('\$${widget.order.amount}'),
            subtitle: Text(DateFormat('dd MM yyyy hh:mm').format(widget.order.dateTime)),
            trailing: IconButton(
              icon: this._isExpended? Icon(Icons.expand_less) : Icon(Icons.expand_more),
              onPressed: (){
                setState(() {
                  this._isExpended = !this._isExpended;
                });
              },
            ),
          ),
          if(_isExpended) Container(
            padding: EdgeInsets.symmetric(
              horizontal: 15,
              vertical: 4
            ),
            height: min(widget.order.products.length*20.0 + 10, 100),
            child: ListView(
              children: widget.order.products
              .map((prod) => Row(children: <Widget>[
                Text(
                  prod.title,
                  style: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.bold,
                  )),
                  SizedBox(width: 8),
                  Text('${prod.quantity} x \$${prod.price}')
              ],))
              .toList(),
            ),
          )
        ]  ,
      ),
    );
  }
}