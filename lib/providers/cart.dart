import 'package:flutter/foundation.dart';
import 'dart:convert';

class CartItem{
  final String id;
  final String title;
  final int quantity;
  final double price;

  CartItem({
    @required this.id,
    @required this.title,
    @required this.quantity,
    @required this.price,
  });

  dynamic getJson(){
    return json.encode({
      'id': id,
      'title': title,
      'quantity': quantity,
      'price': quantity
    });
  }
}


class Cart with ChangeNotifier{
  Map<String,CartItem> _items = {};

  Map<String,CartItem> get items{
    return {..._items};
  }

  int get itemsCount{
    return _items == null? 0 : _items.length;
  }

  double get totalAmount {
    var total = 0.0;
    _items.forEach((key, value) { 
      total += value.price;
    });

    return total;
  }

  void addItem(String prodId, String title, double price){
    if(!_items.containsKey(prodId)){
      _items.putIfAbsent(prodId, () => 
          CartItem(
            id: DateTime.now().toString(), 
            title: title, 
            price: price,
            quantity: 1
          ));
    } else {
      _items.update(prodId, (existCardItem) => 
        CartItem(
          id: existCardItem.id,
          title: existCardItem.title,
          price: existCardItem.price,
          quantity: existCardItem.quantity + 1
      ));
    }
    notifyListeners();
  }

  void removeItem(String prodId){
    _items.remove(prodId);
    notifyListeners();
  }

  void removeSingleItem(String productId){
    if(!_items.containsKey(productId)){
      return;
    } else if( _items[productId].quantity > 1){
      _items.update(productId, (existCartItem) => 
            CartItem(
              id: existCartItem.id,
              price: existCartItem.price,
              title: existCartItem.title,
              quantity: existCartItem.quantity - 1
            ));
    } else {
      _items.remove(productId);
    }
    notifyListeners();
  }

  void clear(){
    _items = {};
    notifyListeners();
  }
}