import 'package:flutter/material.dart';
import 'package:myshop/models/http_exception.dart';
import 'product.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;

class Products with ChangeNotifier {
  List<Product> _items = [];

  final String authToken;
  final String userId;

  Products(this.authToken, this.userId, this._items);

  List<Product> get items {
    return [..._items];
  }

  List<Product> get favoritesItems {
    return _items.where((product) => product.isFavorite).toList();
  }

  Product findById(String productId) {
    return _items.firstWhere((product) => product.id == productId);
  }

  Future<void> addProduct(Product product) async {
    final url =
        'https://myshopflutter-1cee7.firebaseio.com/products.json?auth=${authToken}';
    try {
      final response = await http.post(url,
          body: json.encode({
            'title': product.title,
            'imageUrl': product.imageUrl,
            'description': product.description,
            'price': product.price,
            'crearorId': userId
          }));
      final newProduct = Product(
          title: product.title,
          id: json.decode(response.body)['name'],
          price: product.price,
          imageUrl: product.imageUrl,
          description: product.description);
      _items.add(newProduct);
      notifyListeners();
    } catch (e) {
      throw (e);
    }
  }

  Future<void> deleteProduct(String id) async {
    final url =
        'https://myshopflutter-1cee7.firebaseio.com/products/$id.json?auth=${authToken}';
    final existingProductIndex = _items.indexWhere((prod) => prod.id == id);
    var existingProduct = _items[existingProductIndex];
    _items.removeAt(existingProductIndex);
    notifyListeners();
    final response = await http.delete(url);
    if (response.statusCode >= 400) {
      _items.insert(existingProductIndex, existingProduct);
      notifyListeners();
      throw HttpException('Could not delete product.');
    }
    existingProduct = null;
  }

  Future<void> updateProduct(String id, Product updateProduct) async {
    final productIndex = _items.indexWhere((product) => product.id == id);
    if (productIndex >= 0) {
      try {
        final url =
            'https://myshopflutter-1cee7.firebaseio.com/products/$id.json?auth=${authToken}';
        await http.patch(url,
            body: json.encode({
              'title': updateProduct.title,
              'imageUrl': updateProduct.imageUrl,
              'description': updateProduct.description,
              'price': updateProduct.price
            }));
        _items[productIndex] = updateProduct;
        notifyListeners();
      } catch (e) {
        throw (e);
      }
    }
  }

  Future<void> fetchAndSetProducts([bool filterById = false]) async {
    final filterString =
        filterById ? '&orderBy="creatorId"&equalTo="$userId"' : '';
    var url =
        'https://myshopflutter-1cee7.firebaseio.com/products.json?auth=$authToken' +
            filterString;

    try {
      final response = await http.get(url);
      final extratedData = json.decode(response.body) as Map<String, dynamic>;
      if (extratedData == null) {
        _items = [];
        notifyListeners();
        return;
      }

      url =
          'https://myshopflutter-1cee7.firebaseio.com/userFavorites/$userId.json?auth=$authToken';
      final favoriteResponse = await http.get(url);
      final favoriteData =
          json.decode(favoriteResponse.body) as Map<String, dynamic>;

      final List<Product> loadedProducts = [];
      extratedData.forEach((prodId, prodData) {
        loadedProducts.add(Product(
            id: prodId,
            title: prodData['title'],
            description: prodData['description'],
            price: prodData['price'],
            isFavorite:
                favoriteData == null ? false : favoriteData[prodId] ?? false,
            imageUrl: prodData['imageUrl']));
      });

      _items = loadedProducts;
      notifyListeners();
    } catch (e) {
      throw (e);
    }
  }
}
