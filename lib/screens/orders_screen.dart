import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../providers/orders.dart' show Orders;
import '../widgets/app_drawer.dart';
import '../widgets/order_item.dart';
import 'package:provider/provider.dart';

class OrdersScreen extends StatelessWidget {
  static const String routeName = '/orders';

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
        appBar: AppBar(
          title: Text('Your Order'),
        ),
        drawer: AppDrawer(),
        body: FutureBuilder(
          future: Provider.of<Orders>(context,listen: false).fetchAndSetOrders(),
          builder: (ctx, dataSnapshot) {
            if(dataSnapshot.connectionState == ConnectionState.waiting){
              return Center(child: CircularProgressIndicator());
            } else {
              if(dataSnapshot.error != null){
                //handle error here
                return Center(child: Text('An error occured!'));
              } else {
                return Consumer<Orders>(
                  builder: (ctx, ordersData, child) => 
                  ListView.builder(
                    itemCount: ordersData.orders.length,
                    itemBuilder: (ctx, i) => OrderItem(ordersData.orders[i])
                  ));
                }
            }
          })
      );
  }
}